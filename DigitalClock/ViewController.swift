//
//  ViewController.swift
//  DigitalClock
//
//  Created by AlienBrainz_mac4 on 04/12/18.
//  Copyright © 2018 Allien Brainz Software Pvt. Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var background: UIView!
    @IBOutlet weak var Clock: UILabel!
    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet weak var viewSetting: UIView!
    @IBOutlet weak var ClockColorseg: UISegmentedControl!
    @IBOutlet weak var BackColorseg: UISegmentedControl!
    @IBOutlet weak var Datel: UILabel!
    var timer = Timer()
    var timer1 = Timer()
    var time = DateFormatter()
    var date = DateFormatter()
    
    var color = [#colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1),#colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1),#colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1),#colorLiteral(red: 0.4513868093, green: 0.9930960536, blue: 1, alpha: 1),#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        timer  = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        btnSetting.alpha = 0.5
        btnSetting.setTitle("Setting", for: UIControl.State.normal)
        viewSetting.isHidden = true
        btnSetting.layer.cornerRadius = 20
        viewSetting.layer.cornerRadius = 20
    }

    @IBAction func btnSetting(_ sender: Any) {
        if viewSetting.isHidden {
            btnSetting.setTitle("Hide Setting", for: UIControl.State.normal)
            btnSetting.alpha = 1
            viewSetting.isHidden = false
        }else{
            btnSetting.setTitle("Setting", for: UIControl.State.normal)
            btnSetting.alpha = 0.5
            viewSetting.isHidden = true
        }
    }
    
    @objc func updateTime(){
        time.timeStyle = .medium
        date.dateStyle = .full
        Clock.text = time.string(from: Date())
        Datel.text = date.string(from: Date())
    }
    
    @IBAction func ClockBg(_ sender: Any) {
        if ClockColorseg.selectedSegmentIndex == 0{
            timer1.invalidate()
            Clock.textColor = UIColor.black
            Datel.textColor = UIColor.black
        }else if ClockColorseg.selectedSegmentIndex == 1{
            timer1.invalidate()
            Clock.textColor = UIColor.white
            Datel.textColor = UIColor.white
        }else if ClockColorseg.selectedSegmentIndex == 2{
            timer1 = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(randomColor), userInfo: nil, repeats: true)
        }
    
    }
    
    @IBAction func backBg(_ sender: Any) {
        if BackColorseg.selectedSegmentIndex == 0{
            background.backgroundColor = UIColor.black
        }else if BackColorseg.selectedSegmentIndex == 1{
            background.backgroundColor = UIColor.white
        }else if BackColorseg.selectedSegmentIndex == 2{
            background.backgroundColor = UIColor.gray
        }
    }
    
    @objc func randomColor(){
        let number = Int(arc4random_uniform(5))
        Clock.textColor = color[number]
        Datel.textColor = color[number]
    }
}

